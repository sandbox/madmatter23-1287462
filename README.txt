NAME
--------------------------------
Organic Groups - Registration Format


DESCRIPTION
--------------------------------
This module will let you change the format of the 'Groups' field on the User Registration form to:
* Checkbox (default)
* Select List
* Autocomplete Textfield


Future plans for development:
--------------------------------
* Allowing admins to make OG registration mandatory
* Providing a mechanism that would guide a new user through creating exactly 1 new group if they do not join an existing one


INSTRUCTIONS
--------------------------------

* Enable the module at admin/build/modules/list
* Go to the Organic Groups Administration page at admin/og/og and select a registration format in the 'Group Details' fieldset

CREDITS
--------------------------------
* Authored and maintained by Matthew Grasmick (madmatter23 on drupal.org)
* Some development sponsored by Blue Water Media (http://www.bluewatermedia.com)
